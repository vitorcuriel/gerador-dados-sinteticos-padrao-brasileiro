#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <iostream>
#include <fstream>
using namespace std;

#define NUM_NOMES 4
#define TAM_NOME 100
#define TAM_LINHA 500

void imprime(int rotina, int smae, int spai,char nomeFortamado[TAM_LINHA], char mae[NUM_NOMES][TAM_NOME],char resto[TAM_LINHA],FILE * arq){
  fprintf(arq, "ROT:%d-SMAE:%d-SPAI:%d,",rotina, smae,spai );
  printf("resto = %s",resto);
  strcat(nomeFortamado,resto);
  fprintf(arq,"%s , ", nomeFortamado);
  for (int i = 0; i < NUM_NOMES; i++) {
    fprintf(arq,"%s , ", mae[i]);
  }
//  fprintf(arq, "\n");
}

string extraiNome(string linha, int casaInicio, int casaFinal){
  int virgulas = 0;
  int countNomeDesejado =0;
  string nomeDesejado;
  for(int i =0; i < linha.size() ; i++){
    if(virgulas >= casaInicio && virgulas <= casaFinal){
      nomeDesejado += linha[i];
      countNomeDesejado++;
    }else if(virgulas > casaFinal){
      break;
    }
    if(linha[i] == ','){
      virgulas++;

    }
  }
  return nomeDesejado;
}


string formataNome(int rotina, int smae, int spai,string mae, string filho){
  string nomeFortamado;
  cout<<"Rotina escolhida:"<<rotina<<endl;
  cout<<"Sobrenome da Mae escolhido:"<<smae<<endl;
  cout<<"Sobrenome do Pai escolhido:"<<spai<<endl;
  switch (rotina) {
    case 0://N NpM NP*
      nomeFortamado += extraiNome(filho,0,1);
      nomeFortamado += extraiNome(mae,smae,smae);
      nomeFortamado += extraiNome(filho,spai,spai);
      nomeFortamado += "NULL,";
      nomeFortamado += extraiNome(filho,5,8);//implementar rate de ate onde pegar
      break;

    case 1://N NP* NP*
      nomeFortamado += extraiNome(filho,0,1);
      nomeFortamado += extraiNome(filho,2,2);
      nomeFortamado += extraiNome(filho,3,3);
      nomeFortamado += "NULL,";
      nomeFortamado += extraiNome(filho,5,8);
      break;
    case 2://N NP*
      nomeFortamado += extraiNome(filho,0,1);
      nomeFortamado += extraiNome(filho,spai,spai);
      nomeFortamado += "NULL,NULL,";
      nomeFortamado += extraiNome(filho,5,8);
      break;
    case 3://N NpM NP* NP*
      nomeFortamado += extraiNome(filho,0,1);
      nomeFortamado += extraiNome(mae,smae,smae);
      nomeFortamado += extraiNome(filho,2,2);
      nomeFortamado += extraiNome(filho,3,3);
      nomeFortamado += extraiNome(filho,5,8);
      break;
    case 4://N NM NM
      nomeFortamado += extraiNome(filho,0,1);
      nomeFortamado += extraiNome(mae,2,2);
      nomeFortamado += extraiNome(filho,2,2);
      nomeFortamado += "NULL,";
      nomeFortamado += extraiNome(filho,5,8);
      break;
    case 5://N NM(ultimo sobrenome)
      nomeFortamado += extraiNome(filho,0,1);
      nomeFortamado += extraiNome(mae,2,2);
      nomeFortamado += "NULL,NULL,";
      nomeFortamado += extraiNome(filho,5,8);
      break;

  }


  //printf("Nome formatado == %s\n", nomeFortamado);
  cout<<"Nomeformatado =="<<nomeFortamado<<endl;
  return nomeFortamado;
}


void PegaNome(char saida[NUM_NOMES][TAM_NOME],char resto[TAM_NOME],FILE *arq){
  printf("Entrou\n");
  int will =0;
  char linha[TAM_LINHA];
  char nome[NUM_NOMES][TAM_NOME];
  int i,j=0,k=0;
  int virgula=0;
  bool virg = false;
  fgets(linha, TAM_LINHA+1, arq);//Pega primeira linha com nome
  printf("Pegou a linha\n" );
 for(i = 0;i < sizeof(linha)/4 ;i++ ){
   if(linha[i]!='\\'){
    if(linha[i]==','){
      printf("Achou virgula\n" );
        virgula++;
        virg = true;
    }else virg = false;
    //intervalo com nomes a copiar
    if(virgula > 0 && virgula < 5){
      if(virg){
        printf("Dentro de Virg\n");
        nome[virgula-1][j] ='\0';
        j=0;
      }else{
        nome[virgula-1][j] = linha[i];
        j++;
      }
    }
    if(virgula > 5){
      printf("Dentro de resto\n");
      resto[k] = linha[i];
      k++;
    }
   }

 }
 for (int in = 0; in < 4; in++) {
   strcpy(saida[in],nome[in]);
   printf("Passando dados para a saida\n" );
   k++;
 }
}

int main(void){

  ifstream arqMae;
  ifstream arqFilho;
  ofstream arqSaida;

  arqMae.open("teste1.csv");
  arqFilho.open("teste2.csv");
  arqSaida.open("Saida-11.10.csv");


  string filho;
  string mae;

  int rotina;
  int smae;
  int spai;

  srand((unsigned)time(NULL));

  arqSaida<<"rec-gBR,rec-id,given-name,surname,ssurname,tsurname,dataNasc,codMunicipio,codBairro,mae-given-name,mae-surname,mae-ssurname,mae-tsurname\n";
  getline(arqMae,mae);
  getline(arqFilho,filho);
  while(getline(arqMae,mae) && getline(arqFilho,filho)){

    //Sobre nome da mae a ser usado
    rotina = rand()%6;
    smae = 2+(rand()%2);//escolhe um entre os snomes, %3 para garantir que fique dentre a quantidade de snom
    spai = 2+(rand()%2);//somando dois para pular o log e o primeiro nome

    string nomeF;

    cout<<mae<<endl;
    cout<<filho<<endl;
    cout<<extraiNome(filho,1,1)<<endl;
    string nomeFortamado;
    nomeFortamado = formataNome( rotina,  smae,  spai, mae, filho);

    cout<<"Nome da Mae: "<<mae<<endl;
    cout<<"Nome do Pai: "<<filho<<endl;

    arqSaida<<"ROT:"<<rotina<<"-SMAE:"<<smae<<"-SPAI:"<<spai<<",";
    cout<<nomeFortamado<<endl;
    arqSaida<<nomeFortamado<<",";
    arqSaida<<extraiNome(mae,1,4)<<endl;
    //imprime(rotina,smae,spai,nomeFortamado, mae,resto, arqSaida);
    printf("---------------------------------------------------------------------\n");
  }
  arqMae.close();
  arqFilho.close();
  arqSaida.close();


  return 0;
}
