#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define NUM_NOMES 4
#define TAM_NOME 50
#define TAM_LINHA 200

void imprime(char nomeFortamado[TAM_LINHA], char mae[NUM_NOMES][TAM_NOME],FILE * arq){
  fprintf(arq, "%s , , ", nomeFortamado);
  for (int i = 0; i < NUM_NOMES; i++) {
    fprintf(arq, "%s ,", mae[i]);
  }
  fprintf(arq, "\n");
}

void FormataNome(char nomeFortamado[TAM_LINHA],char mae[NUM_NOMES][TAM_NOME], char filho[NUM_NOMES][TAM_NOME]){

  int rotina;
  int smae;
  int spai;

  srand(time(NULL));
  //Sobre nome da mae a ser usado
  rotina = rand()%6;
  smae = (rand()%3)+1;//escolhe um entre os snomes, %3 para garantir que fique dentre a quantidade de snom
  spai = (rand()%3)+1;
  printf("Rotina escolhida:%d\n",rotina );
  printf("Sobrenome da Mae escolhido:%d\n", smae);
  printf("Sobrenome do Pai escolhido:%d\n", spai);
  switch (rotina) {
    case 0://N NpM NP*
      strcpy(nomeFortamado,filho[0]);
      strcat(nomeFortamado,",");
      strcat(nomeFortamado,mae[smae]);
      strcat(nomeFortamado,",");
      strcat(nomeFortamado,filho[spai]);
      break;
    case 1://N NP* NP*
      strcpy(nomeFortamado,filho[0]);
      strcat(nomeFortamado,",");
      strcat(nomeFortamado,filho[1]);
      strcat(nomeFortamado,",");
      strcat(nomeFortamado,filho[2]);
    case 2://N NP*
    strcpy(nomeFortamado,filho[0]);
    strcat(nomeFortamado,",");
    strcat(nomeFortamado,filho[spai]);
      break;
    case 3://N NpM NP* NP*
      strcpy(nomeFortamado,filho[0]);
      strcat(nomeFortamado,",");
      strcat(nomeFortamado,mae[smae]);
      strcat(nomeFortamado,",");
      strcat(nomeFortamado,filho[1]);
      strcat(nomeFortamado,filho[2]);
      break;
    case 4://N NM NM
      strcpy(nomeFortamado,filho[0]);
      strcat(nomeFortamado,",");
      strcat(nomeFortamado,mae[1]);
      strcat(nomeFortamado,",");
      strcat(nomeFortamado,filho[2]);
      break;
    case 5://N NM(ultimo sobrenome)
      strcpy(nomeFortamado,filho[0]);
      strcat(nomeFortamado,",");
      strcat(nomeFortamado,mae[3]);
      break;
  }
  printf("Nome formatado == %s\n", nomeFortamado);

}

void PegaNome(char saida[NUM_NOMES][TAM_NOME],FILE *arq){
 char linha[TAM_LINHA];
 char nome[NUM_NOMES][TAM_NOME];
 int i,j=0;
 int virgula=0;

 fgets(linha, TAM_LINHA+1, arq);//Pega primeira linha com nome

 for(i = 0;i < sizeof(linha)/4 ;i++ ){
   if(linha[i]==','){
     nome[virgula-1][j] ='\0';
     virgula++;
     j=0;
   }else{
     if(virgula>=1){
       nome[virgula-1][j] = linha[i];
       j++;
     }
   }
 }
 for (int i = 0; i < 4; i++) {
   strcpy(saida[i],nome[i]);
 }

}


int main(){
  /*
  char foo[20] = "vitor trentin";
  char teste[5][20];
  for(int i=0; i<20;i++){
    teste[0][i] = foo[i];
  }
  printf("%s\n", teste[0]);
  */
  char arqFilhoNome[TAM_LINHA] = "gerados_nomes_m.csv";
  char arqMaeNome[TAM_LINHA] = "gerados_nomes_mae.csv";
  char arqSaidaNome[TAM_LINHA] = "sn_ligado_mae.csv";

  FILE *arqFilho;
  FILE *arqMae;
  FILE *arqSaida;

  arqFilho = fopen(arqFilhoNome, "r");
  arqMae = fopen(arqMaeNome, "r");
  arqSaida = fopen(arqSaidaNome, "w");

  char linha[TAM_LINHA];
  char filho[NUM_NOMES][TAM_NOME];
  char mae[NUM_NOMES][TAM_NOME];

  char nomeFortamado[TAM_LINHA];

  if(fgets(linha, TAM_LINHA+1, arqMae)!=NULL && fgets(linha, TAM_LINHA+1, arqFilho)!=NULL){
    //  fgets(linha, 150, arqEntrada);//Pega primeira linha com nome
    //printf("%s\n", linha);

    PegaNome(mae,arqMae);

    printf("MAE: ");
    for (int i = 0; i < NUM_NOMES; i++) {
      printf(" %s ", mae[i]);
    }
    printf("\n");

    PegaNome(filho,arqFilho);
    printf("FILHO NAO FORMATADO: ");
    for (int i = 0; i < NUM_NOMES; i++) {
      printf(" %s ", filho[i]);
    }
    printf("\n");

    FormataNome(nomeFortamado,mae,filho);

    imprime(nomeFortamado, mae, arqSaida);


  }

  fclose(arqMae);
  fclose(arqFilho);
  fclose(arqSaida);
  return 0;
}
