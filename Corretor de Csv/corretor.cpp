#import <stdio.h>
#import <stdlib.h>
#import <string.h>

int main(void){
  printf("Insira o nome do arquivo\n");
	char urlE[20];//= "pnome_f.txt";
  gets(urlE);
  char urlS[20] = "new_";
  strcat(urlS,urlE);
  printf("Url de  entrada == %s.\n", urlE);
  printf("Url de saida == %s.\n",urlS);
	char ch;
	FILE *arqE;
  FILE *arqS;

	arqE = fopen(urlE, "r");
  arqS = fopen(urlS, "w");
	if(arqE == NULL)
	    printf("Erro, nao foi possivel abrir o arquivo\n");
	else
	    while( (ch=fgetc(arqE))!= EOF ){
        if(ch == '|'){
          fputc(';',arqS);
        }else{
          fputc(ch,arqS);
        }
      }

	fclose(arqE);
  fclose(arqS);

	return 0;
}
