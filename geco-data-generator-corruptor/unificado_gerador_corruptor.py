# generate-data-english.py - Python module to generate synthetic data based on
#                            English look-up and error tables.
#
# Peter Christen and Dinusha Vatsalan, January-March 2012
# =============================================================================
#
#  This Source Code Form is subject to the terms of the Mozilla Public
#  License, v. 2.0. If a copy of the MPL was not distributed with this
#  file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# =============================================================================

# Import the necessary other modules of the data generator
#
import basefunctions  # Helper functions
import attrgenfunct   # Functions to generate independent attribute values
import contdepfunct   # Functions to generate dependent continuous attribute
                      # values
import generator      # Main classes to generate records and the data set
import corruptor      # Main classes to corrupt attribute values and records

import random

import datetime
import os
import geradorBR

random.seed(42)  # Set seed for random generator, so data generation can be
                 # repeated

# Set the Unicode encoding for this data generation project. This needs to be
# changed to another encoding for different Unicode character sets.
# Valid encoding strings are listed here:
# http://docs.pythonamp3e2ibdf4ij
# euei8*.org/library/codecs.html#standard-encodings
#
unicode_encoding_used = 'ascii'

# The name of the record identifier attribute (unique value for each record).
# This name cannot be given as name to any other attribute that is generated.
#
rec_id_attr_name = 'rec-id'

now = datetime.datetime.now()
output_directory = './saidas/'+str(now.year)+'_'+str(now.month)+'_'+str(now.day)+'/'
if not os.path.exists(output_directory):
    os.makedirs(output_directory)


# Set the file name of the data set to be generated (this will be a comma
# separated values, CSV, file).
#
out_file_name = output_directory+'father_'+str(now.year)+'_'+str(now.month)+'_'+str(now.day)+'.csv'
out_file_name_father = output_directory+'father_'+str(now.year)+'_'+str(now.month)+'_'+str(now.day)+'.csv'
# Set how many original and how many duplicate records are to be generated.
#
num_org_rec = 500
num_dup_rec = 500

# Set the maximum number of duplicate records can be generated per original
# record.
#
max_duplicate_per_record = 4

# Set the probability distribution used to create the duplicate records for one
# original record (possible values are: 'uniform', 'poisson', 'zipf').
#
num_duplicates_distribution = 'stable'

# Set the maximum number of modification that can be applied to a single
# attribute (field).
#
max_modification_per_attr = 1

# Set the number of modification that are to be applied to a record.
#
num_modification_per_record = 5

# Check if the given the unicode encoding selected is valid.
#
basefunctions.check_unicode_encoding_exists(unicode_encoding_used)

# -----------------------------------------------------------------------------
# Define the attributes to be generated (using methods from the generator.py
# module).
#
gname_attr = \
    generator.GenerateFreqAttribute(attribute_name = 'given-name',
                          freq_file_name = 'lookup-files/pt-files/pnome_composto_m.csv',
                          has_header_line = False,
                          unicode_encoding = unicode_encoding_used)

sname_attr = \
    generator.GenerateFreqAttribute(attribute_name = 'surname',
                          freq_file_name = 'lookup-files/pt-files/unome_m.csv',
                          has_header_line = False,
                          unicode_encoding = unicode_encoding_used)
ssname_attr = \
    generator.GenerateFreqAttribute(attribute_name = 'ssurname',
                          freq_file_name = 'lookup-files/pt-files/unome_m.csv',
                          has_header_line = False,
                          unicode_encoding = unicode_encoding_used)
tsname_attr = \
    generator.GenerateFreqAttribute(attribute_name = 'tsurname',
                          freq_file_name = 'lookup-files/pt-files/unome_m.csv',
                          has_header_line = False,
                          unicode_encoding = unicode_encoding_used)
dataNasc_attr = \
    generator.GenerateFreqAttribute(attribute_name = 'dataNasc',
                          freq_file_name = 'lookup-files/pt-files/dataNasc.csv',
                          has_header_line = False,
                          unicode_encoding = unicode_encoding_used)
codMunicipio_attr = \
    generator.GenerateFreqAttribute(attribute_name = 'codMunicipio',
                          freq_file_name = 'lookup-files/pt-files/cod_Municipio.csv',
                          has_header_line = False,
                          unicode_encoding = unicode_encoding_used)
codBairro_attr = \
    generator.GenerateFreqAttribute(attribute_name = 'codBairro',
                          freq_file_name = 'lookup-files/pt-files/cod_bairros.csv',
                          has_header_line = False,
                          unicode_encoding = unicode_encoding_used)

# -----------------------------------------------------------------------------
# Define how the generated records are to be corrupted (using methods from
# the corruptor.py module).

# For a value edit corruptor, the sum or the four probabilities given must
# be 1.0.
#
edit_corruptor = \
    corruptor.CorruptValueEdit(\
          position_function = corruptor.position_mod_normal,
          char_set_funct = basefunctions.char_set_ascii,
          insert_prob = 0.5,
          delete_prob = 0.5,
          substitute_prob = 0.0,
          transpose_prob = 0.0)

edit_corruptor2 = \
    corruptor.CorruptValueEdit(\
          position_function = corruptor.position_mod_uniform,
          char_set_funct = basefunctions.char_set_ascii,
          insert_prob = 0.25,
          delete_prob = 0.25,
          substitute_prob = 0.25,
          transpose_prob = 0.25)

surname_misspell_corruptor = \
    corruptor.CorruptCategoricalValue(\
          lookup_file_name = 'lookup-files/surname-misspell.csv',
          has_header_line = False,
          unicode_encoding = unicode_encoding_used)

ocr_corruptor = corruptor.CorruptValueOCR(\
          position_function = corruptor.position_mod_normal,
          lookup_file_name = 'lookup-files/ocr-variations.csv',
          has_header_line = False,
          unicode_encoding = unicode_encoding_used)

keyboard_corruptor = corruptor.CorruptValueKeyboard(\
          position_function = corruptor.position_mod_normal,
          row_prob = 0.5,
          col_prob = 0.5)

phonetic_corruptor = corruptor.CorruptValuePhonetic(\
          lookup_file_name = 'lookup-files/phonetic-variations.csv',
          has_header_line = False,
          unicode_encoding = unicode_encoding_used)

missing_val_corruptor = corruptor.CorruptMissingValue()

postcode_missing_val_corruptor = corruptor.CorruptMissingValue(\
       missing_val='missing')

given_name_missing_val_corruptor = corruptor.CorruptMissingValue(\
       missing_value='unknown')


# -----------------------------------------------------------------------------
# Define the attributes to be generated for this data set, and the data set
# itself.
#
attr_name_list = ['given-name', 'surname','ssurname','tsurname','dataNasc','codMunicipio','codBairro']

attr_data_list = [gname_attr, sname_attr,ssname_attr,tsname_attr,dataNasc_attr,codMunicipio_attr,codBairro_attr]

# Nothing to change here - set-up the data set generation object.
#
test_data_generator = generator.GenerateDataSet(output_file_name = \
                                          out_file_name,
                                          write_header_line = True,
                                          rec_id_attr_name = rec_id_attr_name,
                                          number_of_records = num_org_rec,
                                          attribute_name_list = attr_name_list,
                                          attribute_data_list = attr_data_list,
                                          unicode_encoding = \
                                                         unicode_encoding_used)

# Define the probability distribution of how likely an attribute will be
# selected for a modification.
# Each of the given probability values must be between 0 and 1, and the sum of
# them must be 1.0.
# If a probability is set to 0 for a certain attribute, then no modification
# will be applied on this attribute.
#
attr_mod_prob_dictionary = {'given-name':0.2,'surname':0.1,
                            'ssurname':0.1,'tsurname':0.2,
                            'dataNasc':0.1,'codMunicipio':0.2,
                            'codBairro':0.1}

 # Define the actual corruption (modification) methods that will be applied on
 # the different attributes.
 # For each attribute, the sum of probabilities given must sum to 1.0.
 #
attr_mod_data_dictionary = {'given-name':[(0.1, edit_corruptor2),
                                            (0.1, ocr_corruptor),
                                            (0.1, keyboard_corruptor),
                                            (0.7, phonetic_corruptor)],
                            'surname':[(0.1, surname_misspell_corruptor),
                                        (0.1, ocr_corruptor),
                                        (0.1, keyboard_corruptor),
                                        (0.7, phonetic_corruptor)],
                            'ssurname':[(0.1, surname_misspell_corruptor),
                                        (0.1, ocr_corruptor),
                                        (0.1, keyboard_corruptor),
                                        (0.7, phonetic_corruptor)],
                            'tsurname':[(0.1, surname_misspell_corruptor),
                                        (0.1, ocr_corruptor),
                                        (0.1, keyboard_corruptor),
                                        (0.7, phonetic_corruptor)],
                             'dataNasc':[(1.0, edit_corruptor2)],
                             'codMunicipio':[(0.8, keyboard_corruptor),
                                         (0.2, missing_val_corruptor),],
                             'codBairro':[(0.1, edit_corruptor),
                                     (0.5, missing_val_corruptor),
                                     (0.4, keyboard_corruptor),
                                     ]}
# Nothing to change here - set-up the data set corruption object
#
test_data_corruptor = corruptor.CorruptDataSet(number_of_org_records = \
                                          num_org_rec,
                                          number_of_mod_records = num_dup_rec,
                                          attribute_name_list = attr_name_list,
                                          max_num_dup_per_rec = \
                                                 max_duplicate_per_record,
                                          num_dup_dist = \
                                                 num_duplicates_distribution,
                                          max_num_mod_per_attr = \
                                                 max_modification_per_attr,
                                          num_mod_per_rec = \
                                                 num_modification_per_record,
                                          attr_mod_prob_dict = \
                                                 attr_mod_prob_dictionary,
                                          attr_mod_data_dict = \
                                                 attr_mod_data_dictionary)

# =============================================================================
# Nothing to change here - set-up the data set corruption object
#
# No need to change anything below here

# Start the data generation process
#
rec_dict = test_data_generator.generate()

assert len(rec_dict) == num_org_rec  # Check the number of generated records

# Corrupt (modify) the original records into duplicate records
#
rec_dict = test_data_corruptor.corrupt_records(rec_dict)

assert len(rec_dict) == num_org_rec+num_dup_rec # Check total number of records

# Write generate data into a file
#
test_data_generator.write()

# End.
# =============================================================================
# Second run of the generator, to generate mother names
out_file_name = output_directory+'mother_names_'+str(now.year)+'_'+str(now.month)+'_'+str(now.day)+'.csv'
out_file_name_mother =  output_directory+'mother_names_'+str(now.year)+'_'+str(now.month)+'_'+str(now.day)+'.csv'
gname_attr = \
    generator.GenerateFreqAttribute(attribute_name = 'given-name',
                          freq_file_name = 'lookup-files/pt-files/pnome_composto_f.csv',
                          has_header_line = False,
                          unicode_encoding = unicode_encoding_used)

sname_attr = \
    generator.GenerateFreqAttribute(attribute_name = 'surname',
                          freq_file_name = 'lookup-files/pt-files/unome_f.csv',
                          has_header_line = False,
                          unicode_encoding = unicode_encoding_used)
ssname_attr = \
    generator.GenerateFreqAttribute(attribute_name = 'ssurname',
                          freq_file_name = 'lookup-files/pt-files/unome_f.csv',
                          has_header_line = False,
                          unicode_encoding = unicode_encoding_used)
tsname_attr = \
    generator.GenerateFreqAttribute(attribute_name = 'tsurname',
                          freq_file_name = 'lookup-files/pt-files/unome_f.csv',
                          has_header_line = False,
                          unicode_encoding = unicode_encoding_used)


attr_data_list = [gname_attr, sname_attr,ssname_attr,tsname_attr,dataNasc_attr,codMunicipio_attr,codBairro_attr]


test_data_generator = generator.GenerateDataSet(output_file_name = \
                                          out_file_name,
                                          write_header_line = True,
                                          rec_id_attr_name = rec_id_attr_name,
                                          number_of_records = num_org_rec,
                                          attribute_name_list = attr_name_list,
                                          attribute_data_list = attr_data_list,
                                          unicode_encoding = \
                                                         unicode_encoding_used)
# Start the data generation process
#
rec_dict = test_data_generator.generate()

assert len(rec_dict) == num_org_rec  # Check the number of generated records

# Corrupt (modify) the original records into duplicate records
#
rec_dict = test_data_corruptor.corrupt_records(rec_dict)

assert len(rec_dict) == num_org_rec+num_dup_rec # Check total number of records

# Write generate data into a file
#
test_data_generator.write()

# End.
# =============================================================================
out_file_name_son = output_directory+'son_'+str(now.year)+'_'+str(now.month)+'_'+str(now.day)+'.csv'
geradorBR.main(out_file_name_mother,out_file_name_father,out_file_name_son)