# generate-data-english.py - Python module to generate synthetic data based on
#                            English look-up and error tables.
#
# Peter Christen and Dinusha Vatsalan, January-March 2012
# =============================================================================
#
#  This Source Code Form is subject to the terms of the Mozilla Public
#  License, v. 2.0. If a copy of the MPL was not distributed with this
#  file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# =============================================================================

# Import the necessary other modules of the data generator
#
import basefunctions  # Helper functions
import attrgenfunct   # Functions to generate independent attribute values
import contdepfunct   # Functions to generate dependent continuous attribute
                      # values
import generator      # Main classes to generate records and the data set
import corruptor      # Main classes to corrupt attribute values and records

import random
random.seed(42)  # Set seed for random generator, so data generation can be
                 # repeated

# Set the Unicode encoding for this data generation project. This needs to be
# changed to another encoding for different Unicode character sets.
# Valid encoding strings are listed here:
# http://docs.python.org/library/codecs.html#standard-encodings
#
unicode_encoding_used = 'ascii'

# The name of the record identifier attribute (unique value for each record).
# This name cannot be given as name to any other attribute that is generated.
#
rec_id_attr_name = 'rec-id'

# Set the file name of the data set to be generated (this will be a comma
# separated values, CSV, file).
#
out_file_name = 'maes_sintetico.csv'

# Set how many original and how many duplicate records are to be generated.
#
num_org_rec = 500
num_dup_rec = 500

# Set the maximum number of duplicate records can be generated per original
# record.
#
max_duplicate_per_record = 3

# Set the probability distribution used to create the duplicate records for one
# original record (possible values are: 'uniform', 'poisson', 'zipf').
#
num_duplicates_distribution = 'zipf'

# Set the maximum number of modification that can be applied to a single
# attribute (field).
#
max_modification_per_attr = 1

# Set the number of modification that are to be applied to a record.
#
num_modification_per_record = 5

# Check if the given the unicode encoding selected is valid.
#
basefunctions.check_unicode_encoding_exists(unicode_encoding_used)

# -----------------------------------------------------------------------------
# Define the attributes to be generated (using methods from the generator.py
# module).
#
gname_attr = \
    generator.GenerateFreqAttribute(attribute_name = 'given-name',
                          freq_file_name = 'lookup-files/pt-files/pnome_mae.csv',
                          has_header_line = False,
                          unicode_encoding = unicode_encoding_used)

sname_attr = \
    generator.GenerateFreqAttribute(attribute_name = 'surname',
                          freq_file_name = 'lookup-files/pt-files/unome_m.csv',
                          has_header_line = False,
                          unicode_encoding = unicode_encoding_used)
ssname_attr = \
    generator.GenerateFreqAttribute(attribute_name = 'ssurname',
                          freq_file_name = 'lookup-files/pt-files/unome_m.csv',
                          has_header_line = False,
                          unicode_encoding = unicode_encoding_used)
tsname_attr = \
    generator.GenerateFreqAttribute(attribute_name = 'tsurname',
                          freq_file_name = 'lookup-files/pt-files/unome_m.csv',
                          has_header_line = False,
                          unicode_encoding = unicode_encoding_used)
# -----------------------------------------------------------------------------
# Define the attributes to be generated for this data set, and the data set
# itself.
#
attr_name_list = ['given-name', 'surname','ssurname','tsurname']

attr_data_list = [gname_attr, sname_attr,ssname_attr,tsname_attr]

# Nothing to change here - set-up the data set generation object.
#
test_data_generator = generator.GenerateDataSet(output_file_name = \
                                          out_file_name,
                                          write_header_line = True,
                                          rec_id_attr_name = rec_id_attr_name,
                                          number_of_records = num_org_rec,
                                          attribute_name_list = attr_name_list,
                                          attribute_data_list = attr_data_list,
                                          unicode_encoding = \
                                                         unicode_encoding_used)

# Define the probability distribution of how likely an attribute will be
# selected for a modification.
# Each of the given probability values must be between 0 and 1, and the sum of
# them must be 1.0.
# If a probability is set to 0 for a certain attribute, then no modification
# will be applied on this attribute.
#
attr_mod_prob_dictionary = {'gender':0.0, 'given-name':0.0}


# Nothing to change here - set-up the data set corruption object
#
# No need to change anything below here

# Start the data generation process
#
rec_dict = test_data_generator.generate()
with open("rec_dict.csv", 'w') as rec_dict_file:
   rec_dict_file.write(str(rec_dict))
assert len(rec_dict) == num_org_rec  # Check the number of generated records

# Corrupt (modify) the original records into duplicate records
#
#rec_dict = test_data_corruptor.corrupt_records(rec_dict)

#assert len(rec_dict) == num_org_rec+num_dup_rec # Check total number of records

# Write generate data into a file
#
test_data_generator.write()

# End.
# =============================================================================
