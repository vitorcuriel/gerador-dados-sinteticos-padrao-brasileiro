import csv
import random

delimiter = ','


def getNameSegment(full_name, start, end=""):
    segment = ""
    if end == "":
        end = len(full_name)
    for name in range(start, end):
        segment += full_name[name] + delimiter
    return segment


def getSonsName(sons_name_routine, mother_surname, father_surname, mother, father):
    nameFormat = {
        # N NpM NP*
        0: getNameSegment(father, 0, 2) + mother[mother_surname] + delimiter + father[father_surname] + delimiter + delimiter + getNameSegment(father, 5),
        # N NP* NP*
        1: getNameSegment(father, 0, 2) + father[2] + delimiter + father[3] + delimiter + delimiter +  getNameSegment(father, 5),
        # N NP*
        2: getNameSegment(father, 0, 2) + father[father_surname] + delimiter + delimiter + delimiter + getNameSegment(father, 5),
        # N NpM NP* NP*
        3: getNameSegment(father, 0, 2) + mother[mother_surname] + delimiter + father[2] + delimiter + father[3] + delimiter + getNameSegment(father, 5),
        # N NM NM
        4: getNameSegment(father, 0, 2) + mother[2] + delimiter + father[2] + delimiter + delimiter + getNameSegment(father, 5),
        # N NM(ultimo sobrenome)
        5: getNameSegment(father, 0, 2) + mother[2] + delimiter + delimiter + delimiter + getNameSegment(father, 5)
    }
    return nameFormat[sons_name_routine]


def brGeneratorLog(sons_name_routine, mother_surname, father_surname):
    return "RT:" + str(sons_name_routine) + "-MS:" + str(mother_surname) + "-FS:" + str(father_surname) + delimiter


def main(generated_file="", son_file=""):
    with open(son_file, 'w') as son_names_file:
        son_names_file.write(
            "rec-gBR,rec-id,given-name,surname,ssurname,tsurname,dataNasc,codMunicipio,codBairro,mother-given-name,mother-surname,mother-ssurname,mother-tsurname,\n")
        with open(generated_file, 'r') as generated_file:
            generated_reader = csv.reader(generated_file, delimiter=delimiter, quotechar='|')
            generated_reader.next()
            for generated_row in generated_reader:
                sons_name_routine = random.randint(0, 4)
                mother_surname_routine = random.randint(0, 3)
                father_surname_routine = random.randint(2, 4)
                father = []
                for i in range(0,8):
                    father.append(generated_row[i])
                mother = []
                for i in range(8,12):
                    mother.append(generated_row[i])
                print(father)
                print(mother)
                print('-------')
                print(getNameSegment(father, 0, 2))
                print(getNameSegment(father,5))
                print('--------------------------')

                son_name = getSonsName(sons_name_routine, mother_surname_routine, father_surname_routine, mother, father)
                son_names_file.write(brGeneratorLog(sons_name_routine, mother_surname_routine, father_surname_routine)+ son_name +getNameSegment(mother,0)+"\n")





